# Solvers para DEA CCR e BCC
# Aloísio Dourado Neto

from scipy.optimize import linprog
import numpy as np


def CCR_O(X, Y):
    np.warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)
    K, N = X.shape
    K, M = Y.shape

    Aub = np.hstack((Y, -X))
    bub = np.zeros(K)
    beq = np.ones(1)

    ret = []

    for j in range(K):
        Aeq = np.hstack((Y[j], np.zeros(N))).reshape((1, M + N))
        c = np.hstack((np.zeros(M), X[j]))
        resp = linprog(c, A_ub=Aub, b_ub=bub, A_eq=Aeq, b_eq=beq)
        ret.append((resp.x.round(5), round(1 / resp.fun, 5)))

    return ret

def CCR_I(X, Y):
    np.warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)
    K, N = X.shape
    K, M = Y.shape

    Aub = np.hstack((Y, -X))
    bub = np.zeros(K)
    beq = np.ones(1)

    ret = []

    for j in range(K):
        Aeq = np.hstack((np.zeros(M), X[j])).reshape((1, M + N))
        c = np.hstack((-Y[j], np.zeros(N)))
        resp = linprog(c, A_ub=Aub, b_ub=bub, A_eq=Aeq, b_eq=beq)
        ret.append((resp.x.round(5), round(- resp.fun, 5)))

    return ret


def BCC_O(X, Y):
    np.warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)
    K, N = X.shape
    K, M = Y.shape

    Aub = np.hstack((Y, -X, -np.ones((K, 1))))
    bub = np.zeros(K)
    beq = np.ones(1)

    bounds = ([(0, None)] * (N+M))
    bounds.append((None, None))

    ret = []

    for j in range(K):
        Aeq = np.hstack((Y[j], np.zeros(N+1))).reshape((1, M + N + 1))
        c = np.hstack((np.zeros(Y.shape[1]), X[j], np.ones(1)))

        resp = linprog(c, A_ub=Aub, b_ub=bub, A_eq=Aeq, b_eq=beq, bounds=bounds)
        ret.append((resp.x.round(5), round(1 / resp.fun, 5)))


    return ret

def BCC_I(X, Y):
    np.warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)
    K, N = X.shape
    K, M = Y.shape

    Aub = np.hstack((Y, -X, -np.ones((K, 1))))
    bub = np.zeros(K)
    beq = np.ones(1)

    bounds = ([(0, None)] * (N+M))
    bounds.append((None, None))

    ret = []

    for j in range(K):
        Aeq = np.hstack((np.zeros(M), X[j], np.zeros(1))).reshape((1, M + N + 1))
        c = np.hstack((-Y[j], np.zeros(N), np.ones(1)))
        resp = linprog(c, A_ub=Aub, b_ub=bub, A_eq=Aeq, b_eq=beq, bounds=bounds)
        ret.append((resp.x.round(5), round(- resp.fun, 5)))

    return ret
